import { defineBackend } from '@aws-amplify/backend';
import { auth } from './auth/resource';
import { data } from './data/resource';
import { CfnUserPoolGroup } from 'aws-cdk-lib/aws-cognito';

const backend = defineBackend({
  auth,
  data,
});

// extract L1 CfnUserPool resources
const { cfnUserPool } = backend.auth.resources.cfnResources;

// create admin user pool group
const adminGroup = new CfnUserPoolGroup(backend.createStack("CfnUserPoolGroup"), "AdminGroup", {
  userPoolId: cfnUserPool.attrUserPoolId,

  // the properties below are optional
  description: 'group to house admin users',
  groupName: 'admin',
});
